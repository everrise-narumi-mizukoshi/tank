package tank;

import static org.junit.Assert.*;

import org.junit.Test;

public class FuelTankTest {

	@Test
	public void testFuelTank() {
		FuelTank fuelTank = new FuelTank(300);
		assertEquals(300, fuelTank.getFuel());
	}

	@Test
	public void testCheckFuel() {
		FuelTank fuelTank = new FuelTank(500);

		assertTrue(fuelTank.checkFuel(50));
		assertEquals(450, fuelTank.getFuel());

		assertFalse(fuelTank.checkFuel(460));
		assertEquals(450, fuelTank.getFuel());

		assertTrue(fuelTank.checkFuel(450));
		assertEquals(0, fuelTank.getFuel());
	}

	@Test
	public void testGetWeight() {
		FuelTank fuelTank = new FuelTank(400);
		assertEquals(400, fuelTank.getWeight());
	}

	@Test
	public void testFuelSupply() {
		FuelTank fuelTank = new FuelTank(500);
		assertFalse(fuelTank.fuelSupply(30));
		assertEquals(500, fuelTank.getFuel());

		fuelTank.setFuel(450);
		assertFalse(fuelTank.fuelSupply(60));
		assertEquals(450, fuelTank.getFuel());

		fuelTank.setFuel(300);
		assertTrue(fuelTank.fuelSupply(100));
		assertEquals(400, fuelTank.getFuel());

		assertTrue(fuelTank.fuelSupply(0));
		assertEquals(400, fuelTank.getFuel());

	}

}
