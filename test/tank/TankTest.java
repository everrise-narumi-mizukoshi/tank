package tank;

import static org.junit.Assert.*;

import org.junit.Test;


public class TankTest {

	@Test
	public void testStartEngine() {
		Tank tank = new Tank();

		// エンジン停止,燃料満タン:エンジン動く
		tank.setRunEngine(false);
		tank.getFuelTank().setFuel(500);
		assertTrue(tank.startEngine());

		// エンジン停止,燃料ピッタリ：エンジン動く
		tank.setRunEngine(false);
		tank.getFuelTank().setFuel(10);
		assertTrue(tank.startEngine());

		// エンジン停止,燃料足りない：エンジン動かない
		tank.setRunEngine(false);
		tank.getFuelTank().setFuel(5);
		assertFalse(tank.startEngine());

	}

	@Test
	public void testStopEngine() {
		Tank tank = new Tank();
		tank.setRunEngine(true);
		assertFalse(tank.stopEngine());
	}

	@Test
	public void testIsRunEngine() {
		Tank tank = new Tank();
		tank.setRunEngine(true);
		assertTrue(tank.isRunEngine());

		tank.setRunEngine(false);
		assertFalse(tank.isRunEngine());
	}

	@Test
	public void testGetFuelTank() {
		Tank tank = new Tank();
		assertNotNull(tank.getFuelTank());
		assertEquals(500, tank.getFuelTank().getFuel());
	}

	@Test
	public void testHit() {
		Tank tank = new Tank();
		tank.setHp(100);
		tank.hit();
		assertEquals(99, tank.getHp());
		assertFalse(tank.isRunEngine());

		tank.setHp(1);
		tank.hit();
		assertEquals(0, tank.getHp());
		assertFalse(tank.isRunEngine());
	}

	@Test
	public void testGetCannon() {
		Tank tank = new Tank();
		assertNull(tank.getCannon());

		tank.setCannon(new Cannon());
		assertNotNull(tank.getCannon());
		assertEquals(tank.getFuelTank(), tank.getCannon().getFuelTank());
	}

	@Test
	public void testGoForward() {
		// 北向き
		Tank tank = new Tank();
		// エンジンスタートしてない
		tank.goForward(30);
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getY());

		// エンジンスタート
		tank.startEngine();
		// 燃料 前進前：490 前進後：490, distance = 0
		tank.goForward(0);
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getY());
		assertEquals(490, tank.getFuelTank().getFuel());

		// 燃料 前進前：490 前進後：487, distance = 3(00)
		tank.goForward(3);
		assertEquals(300, tank.getY());
		assertEquals(0, tank.getX());
		assertEquals(487, tank.getFuelTank().getFuel());

		// 燃料 前進前：487 前進後：0, distance = 487(00))
		tank.goForward(487);
		assertEquals(49000, tank.getY());
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getFuelTank().getFuel());

		// 燃料 前進前：0 前進後：0, distance = 5(00) ※進まない
		tank.goForward(5);
		assertEquals(49000, tank.getY());
		assertEquals(0, tank.getX());
	}

	@Test
	public void testGoBack() {
		// 北向き
		Tank tank = new Tank();
		// エンジンスタートしてない
		tank.goBack(30);
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getY());

		// エンジンスタート
		tank.startEngine();
		// 燃料 前進前：490 前進後：490, distance = 0
		tank.goBack(0);
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getY());
		assertEquals(490, tank.getFuelTank().getFuel());

		// 燃料 前進前：490 前進後：487, distance = 3(00)
		tank.goBack(3);
		assertEquals(-300, tank.getY());
		assertEquals(0, tank.getX());
		assertEquals(487, tank.getFuelTank().getFuel());

		// 燃料 前進前：487 前進後：0, distance = 487(00))
		tank.goBack(487);
		assertEquals(-49000, tank.getY());
		assertEquals(0, tank.getX());
		assertEquals(0, tank.getFuelTank().getFuel());

		// 燃料 前進前：0 前進後：0, distance = 5(00) ※進まない
		tank.goBack(5);
		assertEquals(-49000, tank.getY());
		assertEquals(0, tank.getX());

	}

	@Test
	public void testTurnRight90() {
		// 北向き
		Tank tank = new Tank();
		// エンジンスタートしてない
		tank.turnRight90();
		assertEquals(Direction.N, tank.direction);

		// エンジンスタート
		tank.startEngine();
		// 燃料 前：490 右90：東 後：489
		tank.turnRight90();
		assertEquals(Direction.E, tank.direction);
		assertEquals(489, tank.getFuelTank().getFuel());

		// 前進
		tank.goForward(3);
		assertEquals(300, tank.getX());
		assertEquals(0, tank.getY());

		// もう90度
		tank.turnRight90();
		assertEquals(Direction.S, tank.direction);

		// 燃料足りぬ
		tank.getFuelTank().setFuel(0);
		tank.turnRight90();
		assertEquals(Direction.S, tank.direction);
	}

	@Test
	public void testTurnLeft90() {
		// 北向き
		Tank tank = new Tank();
		// エンジンスタートしてない
		tank.turnRight90();
		assertEquals(Direction.N, tank.direction);

		// エンジンスタート
		tank.startEngine();
		// 燃料 前：490 右90：西 後：489
		tank.turnLeft90();
		assertEquals(Direction.W, tank.direction);
		assertEquals(489, tank.getFuelTank().getFuel());

		// 前進
		tank.goForward(3);
		assertEquals(-300, tank.getX());
		assertEquals(0, tank.getY());

		// もう90度
		tank.turnLeft90();
		assertEquals(Direction.S, tank.direction);

		// 燃料足りぬ
		tank.getFuelTank().setFuel(0);
		tank.turnLeft90();
		assertEquals(Direction.S, tank.direction);
	}

	@Test
	public void testTurn180() {
		// 北向き
		Tank tank = new Tank();
		// エンジンスタートしてない
		tank.turn180();
		assertEquals(Direction.N, tank.direction);

		// エンジンスタート
		tank.startEngine();
		// 燃料 前：490 右90：西 後：489
		tank.turn180();
		assertEquals(Direction.S, tank.direction);
		assertEquals(488, tank.getFuelTank().getFuel());

		// 前進
		tank.goForward(3);
		assertEquals(0, tank.getX());
		assertEquals(-300, tank.getY());

		// もう90度
		tank.turn180();
		assertEquals(Direction.N, tank.direction);

		// 燃料足りぬ
		tank.getFuelTank().setFuel(0);
		tank.turn180();
		assertEquals(Direction.N, tank.direction);
	}

	@Test
	public void testFire() {
		Tank tank = new Tank();
		// 砲塔が設置されていない
		assertNull(tank.fire());

		// 砲塔設置
		tank.setCannon(new Cannon());
		// 弾なし
		assertNull(tank.fire());

		// 弾設置
		tank.getCannon().addCannonBall(new CannonBallA());
		CannonBall ball = tank.fire();
		assertNotNull(ball);
		assertEquals(0, ball.getFireX());
		assertEquals(1000, ball.getFireY());
	}

	@Test
	public void testCalcGrossWeight() {
		Tank tank = new Tank();
		// 砲塔無し、人なし
		assertEquals(10500, tank.calcGrossWeight());

		// 砲塔あり、弾なし、人なし
		tank.setCannon(new Cannon());
		assertEquals(10500, tank.calcGrossWeight());

		// 砲弾10発(A:5, B:5)、人なし
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallB());
		assertEquals(10900, tank.calcGrossWeight());

		// 人2人
		tank.setGunner(new Gunner("Tom", 50));
		tank.setSteerer(new Steerer("Jun", 80));
		assertEquals(11030, tank.calcGrossWeight());

	}

}
