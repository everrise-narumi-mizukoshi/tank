package tank;

public class CannonBallB extends CannonBall{
	private static final int WEIGHT = 30;
	private static final int FRYING_DISTANCE = 2000;
	private static final int USE_FUEL = 1;

	@Override
	public int getWeight() {
		return WEIGHT;
	}
	@Override
	public int getFryingDistance() {
		return FRYING_DISTANCE;
	}
	@Override
	public int getUseFuel() {
		return USE_FUEL;
	}

}
