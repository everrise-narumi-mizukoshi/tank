package tank;

public class Main {

	public static void main(String[] args) {
		Tank tank = new Tank();
		tank.setCannon(new Cannon());
		// 弾こめる
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallB());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallA());
		tank.getCannon().addCannonBall(new CannonBallB());

		// 総舵手
		Steerer steererA = new Steerer("A", 50);
		Steerer steererB = new Steerer("B", 60);
		Steerer steererC = new Steerer("C", 70);

		// 砲手
		Gunner gunnerD = new Gunner("D", 80);
		Gunner gunnerE = new Gunner("E", 50);

		// 乗せる
		tank.setSteerer(steererB);
		tank.setGunner(gunnerE);

		// 00
		tank.startEngine();
		// 01
		tank.goForward(3);
		// 02
		tank.turnRight90();
		// 03
		tank.goForward(15);
		// 04
		tank.turnLeft90();
		// 05
		tank.goForward(3);
		// 06
		tank.turnRight90();
		// 07
		tank.goForward(1);
		// 08
		tank.turnLeft90();
		// 09
		tank.goBack(4);
		// 10
		tank.hit();
		if (!tank.isRunEngine()) {
			tank.startEngine();
		}
		// 11
		tank.turnLeft90();
		System.out.println("燃料：" + tank.getFuelTank().getFuel());

		// 12 燃料補給所に到着
		tank.goForward(8);
		System.out.println("燃料補給所です");
		// 13
		tank.getFuelTank().fuelSupply(20);
		// 14
		tank.turnLeft90();
		// 15
		tank.goForward(4);
		// 16
		tank.turn180();
		// 17
		tank.goBack(4);
		System.out.println("燃料：" + tank.getFuelTank().getFuel());
		// 18
		tank.getCannon().turn(90);
		// 19
		tank.fire();
		// 20
		tank.turnRight90();
		// 21
		tank.goForward(10);
		System.out.println("橋到着");
		System.out.println("総重量：" + tank.calcGrossWeight());
		// 22
		tank.goForward(1);
		System.out.println("橋わたりきった");
		System.out.println("燃料：" + tank.getFuelTank().getFuel());
		// 23
		tank.getCannon().turn(90);
		System.out.println("燃料：" + tank.getFuelTank().getFuel());
		// 24
		tank.fire();
		System.out.println("燃料：" + tank.getFuelTank().getFuel());
		// 25
		tank.fire();
		// 26
		tank.getCannon().turn(270);
		// 27
		tank.fire();
		// 28
		tank.goForward(10);
		// 29
		tank.hit();
		if (!tank.isRunEngine()) {
			tank.startEngine();
		}
		// 30
		tank.turnRight90();
		// 31
		tank.goForward(9);
		System.out.println("燃料：" + tank.getFuelTank().getFuel());
		System.out.println("総重量：" + tank.calcGrossWeight());
		System.out.println("戦車向き:" + tank.direction.getName());
		System.out.println("砲塔向き:" + tank.getCannon().direction);
		System.out.println("あと" + tank.getHp() + "発耐えられます.");

	}

}
