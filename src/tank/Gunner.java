package tank;

public class Gunner {
	private String name;
	private int weight;

	public Gunner(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return this.name;
	}

	public int getWeight() {
		return this.weight;
	}

}
