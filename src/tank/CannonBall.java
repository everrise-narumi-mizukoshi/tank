package tank;

public abstract class CannonBall {
	private int fireX;
	private int fireY;
	private Direction fireDir;

	public abstract int getWeight();

	public abstract int getFryingDistance();

	public abstract int getUseFuel();

	public void fire(int x, int y, Direction dir) {
		this.fireX = x;
		this.fireY = y;
		this.fireDir = dir;
	}

	public int getFireX() {
		return fireDir.getX() * getFryingDistance() + fireX;
	}

	public int getFireY() {
		return fireDir.getY() * getFryingDistance() + fireY;
	}


}
