package tank;

public class CannonBallA extends CannonBall{
	private static final int WEIGHT = 50;
	private static final int FRYING_DISTANCE = 1000;
	private static final int USE_FUEL = 2;

	@Override
	public int getWeight() {
		return WEIGHT;
	}
	@Override
	public int getFryingDistance() {
		return FRYING_DISTANCE;
	}
	@Override
	public int getUseFuel() {
		return USE_FUEL;
	}

}
