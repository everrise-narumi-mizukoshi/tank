package tank;

/**
 * 戦車クラス.
 * @author narumi
 */
public class Tank {
	private static final int USE_FUEL_FOR_START_ENGINE = 10;
    /** 方角：初期 北 向いてる. */
    Direction direction = Direction.N;

    /**
     * 位置.
     * 戦車の動きは2次元のため、xとyを準備.
     */
    private int x = 0;
    private int y = 0;

    /**
     * 燃料タンク.
     * 初期：500L
     */
    private FuelTank fuelTank = new FuelTank(500);

    /** 戦車の重さ(t). */
    private int tankWeight = 10 * 1000;

    /** 戦車のHP(0になると行動不能). */
    private int hp = 10;

    /** エンジン動いてるかフラグ. */
    private boolean runEngine = false;

    /** 砲塔. */
    private Cannon cannon;

    /** 総舵手. */
    private Steerer steerer;

    /** 砲手. */
    private Gunner gunner;

    /**
     * エンジン始動.
     */
    public boolean startEngine() {
	    if (fuelTank.checkFuel(USE_FUEL_FOR_START_ENGINE)) {
	    	this.runEngine = true;
	        System.out.println("エンジン始動.");
	        return true;
	    } else {
	    	this.runEngine = false;
	        System.out.println("燃料が足りずエンジンを始動できません.");
	        return false;
	    }
    }

    /**
     * エンジン停止.
     */
    public boolean stopEngine() {
    	this.runEngine = false;
    	System.out.println("エンジン停止.");
    	return this.runEngine;
    }

    /**
     * エンジン動作チェック.
     * @return
     */
    public boolean isRunEngine() {
    	return this.runEngine;
    }

    /**
     * エンジンフラグ設定.
     * @param runEngine
     */
    public void setRunEngine(boolean runEngine) {
    	this.runEngine = runEngine;
    }

    /**
     * 燃料タンク取得.
     * @return
     */
    public FuelTank getFuelTank() {
    	return this.fuelTank;
    }

    /**
     * 被弾.
     */
    public void hit() {
    	System.out.println("被弾しました.");
    	hp--;
    	stopEngine();
    	if (hp == 0) {
    		System.out.println("行動不能です.");
    	}
    }

    /**
     * 砲塔ゲット.
     * @return
     */
	public Cannon getCannon() {
		return cannon;
	}

	/**
	 * 砲塔セット.
	 * @param cannon
	 */
	public void setCannon(Cannon cannon) {
		this.cannon = cannon;
		this.cannon.fuelConnectTank(this.fuelTank);
	}


    /**
     * 前進する
     * 単位は100mづつ.
     * 方角を考慮する必要がある.
     * @param distance 距離
     */
    public void goForward(int distance) {
    	if (!isRunEngine()) {
        	System.out.println("エンジンが停止しています.");
        	return;
        } else if (!fuelTank.checkFuel(distance)) {
            System.out.println("燃料が足りません.");
            return;
        }

        this.x += this.direction.getX() * (distance * 100);
        this.y += this.direction.getY() * (distance * 100);

        System.out.println(this.direction.getName() + "に"
                + Integer.toString(distance * 100) + "m 前進しました");
        System.out.println("現在位置は x:" + Integer.toString(this.x)
                + " y:" + Integer.toString(this.y));
    }

    /**
     * 後退する
     * 単位は100mづつ.
     * @param distance 距離
     */
    public void goBack(int distance) {
    	if (!isRunEngine()) {
        	System.out.println("エンジンが停止しています.");
        	return;
        } else if (!fuelTank.checkFuel(distance)) {
            System.out.println("燃料が足りません.");
            return;
        }

        this.x -= this.direction.getX() * (distance * 100);
        this.y -= this.direction.getY() * (distance * 100);

        System.out.println(this.direction.getName() + "に"
                + Integer.toString(distance * 100) + "m 後退しました");
        System.out.println("現在位置は x:" + Integer.toString(this.x)
                + " y:" + Integer.toString(this.y));
    }

    /**
     * 右に90度回転.
     */
    public void turnRight90() {
    	if (!isRunEngine()) {
        	System.out.println("エンジンが停止しています.");
        	return;
        } else if (!fuelTank.checkFuel(1)) {
            System.out.println("燃料が足りません.");
            return;
        }
        this.direction = this.direction.right();
    }

    /**
     * 左に90度回転.
     */
    public void turnLeft90() {
    	if (!isRunEngine()) {
        	System.out.println("エンジンが停止しています.");
        	return;
        } else if (!fuelTank.checkFuel(1)) {
            System.out.println("燃料が足りません.");
            return;
        }

        this.direction = this.direction.left();
    }

    /**
     * 180度回転.
     */
    public void turn180() {
    	if (!isRunEngine()) {
        	System.out.println("エンジンが停止しています.");
        	return;
        } else if (!fuelTank.checkFuel(2)) {
            System.out.println("燃料が足りません.");
            return;
        }

        this.direction = this.direction.back();
    }

    /**
     * 発射させる.
     */
    public CannonBall fire() {
    	if (cannon == null) {
    		return null;
    	}
    	CannonBall ball = cannon.fire(x, y, direction);
    	if (ball == null) {
    		return null;
    	}
    	System.out.println("砲弾の位置：x-" + ball.getFireX()
    			+ " y-" + ball.getFireY());
    	return ball;
    }

	/**
	 * 総重量.
	 * @return
	 */
	public int calcGrossWeight() {
		int sumAll = 0;
		int cannonBallWeight = 0;
		if (cannon != null) {
			for (CannonBall cannonBall : cannon.getCannonBallQueue()) {
				cannonBallWeight += cannonBall.getWeight();
			}
		} else {
			System.out.println("砲塔が設置されていません.");
		}
		sumAll += tankWeight + cannonBallWeight;
		if (steerer != null) {
			sumAll += steerer.getWeight();
		}
		if (gunner != null) {
			sumAll += gunner.getWeight();
		}
		if (fuelTank != null) {
			sumAll += fuelTank.getWeight();
		}
		return sumAll;
	}


	public Steerer getSteerer() {
		return steerer;
	}

	public void setSteerer(Steerer steerer) {
		this.steerer = steerer;
	}

	public Gunner getGunner() {
		return gunner;
	}

	public void setGunner(Gunner gunner) {
		this.gunner = gunner;
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}




}
