package tank;

/**
 * 燃料クラス.
 * @author narumi.mizukoshi
 *
 */
public class FuelTank {

    /** 燃料タンクのキャパ(L). */
    private static final int TANK_CAPACITY = 500;
    /** 燃料. */
    private int fuel;

    /**
     * 燃料タンクコンストラクタ.
     * @param fuel 燃料 (L)
     */
    public FuelTank(int fuel) {
        this.fuel = fuel;
    }

    /**
     * 燃料の量取得.
     * @return
     */
    public int getFuel() {
        return this.fuel;
    }

    public void setFuel(int fuel) {
    	this.fuel = fuel;
    }

    /**
     * 十分な燃料があるかチェックする.
     * @param useFuel 使用したい燃料.
     * @return
     */
    public boolean checkFuel(int useFuel) {
        if (this.fuel < useFuel) {
            return false;
        }
        this.fuel -= useFuel;
        return true;
    }

    /**
     * 燃料タンクの重さ取得.
     * @return
     */
    public int getWeight() {
    	return this.fuel;
    }

    /**
     * 給油.
     * @param fuel 燃料.
     * @return
     */
    public boolean fuelSupply(int fuel) {
    	if (this.fuel + fuel <= TANK_CAPACITY) {
    		this.fuel += fuel;
    		return true;
    	}
    	return false;
    }

}
