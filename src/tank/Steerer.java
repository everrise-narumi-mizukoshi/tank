package tank;

public class Steerer {
	private String name;
	private int weight;

	public Steerer(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return this.name;
	}

	public int getWeight() {
		return this.weight;
	}

}
