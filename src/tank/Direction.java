package tank;


/**
 * 方角.
 * @author narumi
 */
public enum Direction {
    N(0, 1, "北"),
    E(1, 0, "東"),
    W(-1, 0, "西"),
    S(0, -1, "南");
    // 日本語での表示名称
    private String name;
    private int x;
    private int y;

    private Direction right;
    private Direction left;
    private Direction back;

    private Direction(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    static {
        N.right = E;
        N.left = W;
        N.back = S;

        E.right = S;
        E.left = N;
        E.back = W;

        W.right = N;
        W.left = S;
        W.back = E;

        S.right = W;
        S.left = E;
        S.back = N;
    }

    public String getName() {
        return this.name;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Direction right() {
        return this.right;
    }

    public Direction left() {
        return this.left;
    }

    public Direction back() {
        return this.back;
    }
}
