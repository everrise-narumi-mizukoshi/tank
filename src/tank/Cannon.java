package tank;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 砲塔.
 * @author narumi.mizukoshi
 *
 */
public class Cannon {
    /** 方角：初期 正面(北) 向いてる. */
    public int direction = 0;
    /** 燃料. */
    private FuelTank fuelTank;
    /** 砲弾入れ. */
    private Queue<CannonBall> cannonBallQueue = new LinkedList<CannonBall>();

    /**
     * 右方向にくるくる回る.
     * 360度以上にはしない.
     * @param turnAngle 角度
     */
    public void turn(int turnAngle) {
    	fuelTank.checkFuel(turnAngle / 90);
    	this.direction = turnAngle % 360;
    }

    /**
     * 燃料つなぎこみ.
     * @param fuelTank
     */
    public void fuelConnectTank(FuelTank fuelTank) {
    	this.fuelTank = fuelTank;
    }

    public FuelTank getFuelTank() {
    	return this.fuelTank;
    }

    /**
     * 砲弾.
     */
    public CannonBall fire(int tankX, int tankY, Direction tankDir) {
    	if (cannonBallQueue == null || cannonBallQueue.size() == 0) {
    		System.out.println("弾がなくて撃てません.");
    		return null;
    	}
    	CannonBall ball = cannonBallQueue.poll();
    	if (!fuelTank.checkFuel(ball.getUseFuel())) {
    		return null;
    	}
    	System.out.println("どーん！！！");
    	ball.fire(tankX, tankY, getCannonDir(tankDir));
    	fuelTank.checkFuel(ball.getUseFuel());
    	return ball;
    }

    /**
     * 弾セット.
     * @param cannonBall
     */
    public void addCannonBall(CannonBall cannonBall) {
    	if (cannonBallQueue.size() >= 10) {
    		System.out.println("弾がいっぱいです.");
    		return;
    	}
    	cannonBallQueue.add(cannonBall);
    }

    /**
     * キュー取得.
     * @return
     */
    public Queue<CannonBall> getCannonBallQueue() {
    	return this.cannonBallQueue;
    }

    /**
     * タンクの方角から砲塔の向いている方角取得.
     * @param tankDir
     * @return
     */
    private Direction getCannonDir(Direction tankDir) {
    	int mod = this.direction % 90;
    	Direction cannonDir = null;
    	switch (mod) {
	    	case 0:
	    		cannonDir = tankDir;
	    		break;
	    	case 1:
	    		cannonDir = tankDir.right();
	    		break;
	    	case 2:
	    		cannonDir = tankDir.back();
	    		break;
	    	case 3:
	    		cannonDir = tankDir.left();
	    		break;
    	}
    	return cannonDir;
    }

}
